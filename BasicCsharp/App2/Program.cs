﻿using System;

namespace Codementors.BasicCsharp.App2
{
    class Program
    {
        /// <summary>
        /// Main code of application
        /// </summary>
        /// <remarks>This is run on the start and application will close, when it will hit the end</remarks>
        /// <param name="args">command line parameters - not used here</param>
        static void Main(string[] args)
        {
            // define variables (zmienne)
            int number = 42;
            string text = "some text";
            double real = 3.141592653589793;

            // print variables
            Console.WriteLine(@"number = {number}");
            Console.WriteLine($"real = {real}");
            Console.WriteLine($"text = {text}");

            // read new values
            Console.Write("Podaj nowy string: ");
            text = Console.ReadLine();

            Console.Write("Podaj nową liczbę całkowitą: ");
            number = Convert.ToInt32(Console.ReadLine());

            Console.Write("Podaj nową liczbę rzeczywistą: ");
            real = Convert.ToDouble(Console.ReadLine());

            // print variables
            Console.WriteLine(@"number = {number}");
            Console.WriteLine($"real = {real}");
            Console.WriteLine($"text = {text}");
        }
    }
}
