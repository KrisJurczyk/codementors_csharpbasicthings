﻿namespace Codementors.BasicCsharp.App11_structs
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
#region int

            int a = 42;
            int b = a;

            Console.WriteLine($"a = {a}");
            Console.WriteLine($"b = {b}");

            a = 44;

            Console.WriteLine($"a = {a}");
            Console.WriteLine($"b = {b}");

            Console.WriteLine("**************************************************");

            #endregion

#region array

            int[] c = new int[] { 42 };
            int[] d = c;

            Console.WriteLine($"c = {c[0]}");
            Console.WriteLine($"d = {d[0]}");

            c[0] = 44;

            Console.WriteLine($"c = {c[0]}");
            Console.WriteLine($"d = {d[0]}");
            #endregion

#region null
            c = null;
            int? e = null; // int e = null nie zadziała
            string f = null;
            Func<int> g = null;
#endregion
        }
    }
}
