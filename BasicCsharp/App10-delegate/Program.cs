﻿using System;
using System.Collections.Generic;
using Codementors.BasicCsharp.App8_enums;

namespace Codementors.BasicCsharp.App10_delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            char operationChar = Console.ReadKey().KeyChar;

            // this dictionary maps chars to delegates (double, double) => double
            var charToOperation = new Dictionary<char, Func<double, double, double>>
            {
                ['+'] = (a, b) => a+b,
                ['-'] = (a, b) => a-b,
                ['*'] = (a, b) => a*b,
                ['x'] = (a, b) => a*b,
                ['/'] = (a, b) => a/b,
                ['^'] = Math.Pow,           //Math.Pow is already a function (double, double) => double
                ['%'] = (a, b) => a%b,
            };

            Func<double, double, double> operation;

            if(charToOperation.TryGetValue(operationChar, out operation))
            {
                // invokes delegate stored in 'operation'
                int result = (int)operation(first, second);

                Console.WriteLine();
                Console.WriteLine($"{first} {operationChar} {second} = {result}");
            }
            else
                Console.Error.WriteLine("Nieznana operacja");
        }
    }
}
