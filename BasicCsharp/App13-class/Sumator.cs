﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codementors.BasicCsharp.App13_class
{
    public class Sumator
    {
        private readonly int[] data;

        private int i = 0;

        private int sum = 0;
        private int min = int.MaxValue;

        public Sumator(int count)
        {
            data = new int[count];
        }

        public void AddNumber(int number)
        {
            data[i++] = number;

            this.sum += number;
            this.min = Math.Min(this.min, number);
            this.Max = Math.Max(this.Max, number);
        }

        public int GetSum()
        {
            return sum;
        }

        public int Min
        {
            get
            {
                return this.min;
            }
        }

        public int Max { get; private set; } = int.MinValue;
    }
}
