﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codementors.BasicCsharp.App13_class
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ile liczb chcesz zsumować: ");
            int count = App4_sumator.Program.ReadInt();

            // initialize sumator object
            var sumator = new Sumator(count);

            //read data
            for (int i = 0; i < count; i++)
            {
                // read array element
                Console.Write($"Podaj {i}-tą liczbę: ");
                sumator.AddNumber(App4_sumator.Program.ReadInt());
            }

            Console.WriteLine();
            Console.WriteLine($"sum = {sumator.GetSum()}");
            Console.WriteLine($"min = {sumator.Min}");
            Console.WriteLine($"max = {sumator.Max}");
        }
    }
}
