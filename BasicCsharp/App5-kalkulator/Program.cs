﻿using System;

namespace Codementors.BasicCsharp.App5_kalkulator
{
    class Program
    {
        static void Main(string[] args)
        {
           
            // todo referencej ReadInt()
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            char operation = Console.ReadKey().KeyChar;

            int result;
            switch (operation)
            {
                case '+':
                    result = first + second;
                    break;

                case '-':
                    result = first - second;
                    break;

                case '/':
                    result = first / second;
                    break;

                case '*':
                case 'x':
                    result = first * second;
                    break;

                case '%':
                    result = first % second;
                    break;

                //todo inne operacje
                default:
                    Console.Error.WriteLine("Nieznana operacja");
                    return;
            }

            Console.WriteLine();
            Console.WriteLine($"{first} {operation} {second} = {result}");
        }
    }
}
