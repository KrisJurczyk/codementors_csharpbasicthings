﻿using System;
using System.Threading;

namespace Codementors.BasicCsharp.App1
{
    class Program
    {
        /// <summary>
        /// Main code of application
        /// </summary>
        /// <remarks>This is run on the start and application will close, when it will hit the end</remarks>
        /// <param name="args">command line parameters - not used here</param>
        static void Main(string[] args)
        {
            // prints some text on a console screen
            Console.WriteLine("Pierwsza aplikacja!");

            /* waits 5 sec 
              or 5000 milliseconds */
            Thread.Sleep(5000);
            Console.WriteLine("Ostatnia linia");

            // waits for any key to be pressed
            Console.ReadKey();
        }
    }
}
