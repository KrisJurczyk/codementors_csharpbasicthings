﻿using System;
using System.Collections.Generic;

namespace Codementors.BasicCsharp.App10_delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            string operationChar = Console.ReadLine();

            var charToOperation = new Dictionary<string, Func<int, int, int>>
            {
                ["+"] = (a, b) => a + b,
                ["-"] = (a, b) => a - b,
                ["*"] = (a, b) => a * b,
                ["/"] = (a, b) => a / b,
                ["%"] = (a, b) => a % b,
                ["&"] = (a, b) => a & b,
                ["^"] = (a, b) => a ^ b,
                ["|"] = (a, b) => a | b,
                ["~"] = (a, b) => ~a,
                ["<<"] = (a, b) => a << b,
                [">>"] = (a, b) => a >> b,
                ["++"] = (a, b) => a++, // ++a ?
                ["--"] = (a, b) => --a, // a-- ?
            };

            Func<int,int,int> operation;

            if (charToOperation.TryGetValue(operationChar, out operation))
            {
                // invokes delegate stored in 'operation'
                int result = operation(first, second);

                Console.WriteLine();
                Console.WriteLine($"{first} {operationChar} {second} = {result}");
            }
            else
                Console.Error.WriteLine("Nieznana operacja");
        }
    }
}
