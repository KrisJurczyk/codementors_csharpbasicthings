﻿using System;
using Codementors.BasicCsharp.App4_sumator;

namespace Codementors.BasicCsharp.App6_loop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();
           
            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            char operation = Console.ReadKey().KeyChar;

            // repeats until shouldStartAgain is false
            // TODO  use do {...} while(shouldStartAgain);
            bool shouldStartAgain = true;
            do
            {
                double result;
                switch (operation)
                {
                    case '+':
                        result = first + second;
                        break;

                    case '-':
                        result = first - second;
                        break;

                    case '/':
                        //todo test it
                        result = first / second;
                        break;

                    case '*':
                    case 'x':
                        result = first * second;
                        break;

                    default:
                        Console.Error.WriteLine("Nieznana operacja");
                        return;
                }

                Console.WriteLine();
                Console.WriteLine($"{first} {operation} {second} = {result}");

                Console.WriteLine("Czy zacząć od nowa? (t/n)");
                char key = Console.ReadKey().KeyChar;
                shouldStartAgain = (key == 't' || key == 'T');
            } while (shouldStartAgain);
        }
    }
}
